#![feature(lang_items, start, asm)]
#![no_std]

pub const SYS_EXIT : usize = 60;
pub const SYS_WRITE : usize = 1;

#[allow(dead_code)]
pub fn sys_exit(code:usize) -> !{
    unsafe{
        asm!("syscall"
             : 
             : "{rax}"(SYS_EXIT), "{rdi}"(code)
             : "rax");
        loop {}
    }
}

pub fn sys_write(fd:isize,
                 buf:*const u8,
                 count:isize) -> isize {
    let ret : isize;
    unsafe{
        asm!("syscall"
             : "={rax}"(ret)
             : "{rax}"(SYS_WRITE),
             "{rdi}"(fd),
             "{rsi}"(buf),
             "{rdx}"(count));
    }
    ret
}

#[start]
#[no_mangle]
pub fn _start(_:isize, _: *const *const u8) -> isize {
    let x = Box::new(5);

    let mut text = b"hello\n";
    sys_write(1, (& mut text).as_ptr(), 6);

    sys_exit(10);
}

#[lang = "eh_personality"] extern fn eh_personality() {}
#[lang = "panic_fmt"] fn panic_fmt() -> ! { loop {} }

